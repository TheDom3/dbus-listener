/* This example creates a D-Bus server with the following functionality:
   It registers the "com.example.dbustest" name, creates a "/hello" object path,
   which has an "com.example.dbustest" interface.
   The interface has a "Hello" method (which takes no arguments and returns a string),
   and a "HelloHappened" signal (with a string argument) which is sent every time
   someone calls the "Hello" method.
*/
extern crate dbus;
extern crate dbus_tree;

use dbus::blocking::LocalConnection;
use dbus_tree::Factory;
use std::error::Error;
use std::sync::Arc;
use std::time::Duration;

fn main() -> Result<(), Box<dyn Error>> {
    // Let's start by starting up a connection to the session bus and request a name.
    let c = LocalConnection::new_system()?;
    c.request_name("org.freedesktop.login1", false, true, false)?;

    // The choice of factory tells us what type of tree we want,
    // and if we want any extra data inside. We pick the simplest variant.
    let f = Factory::new_fn::<()>();

    // We create the signal first, since we'll need it in both inside the method callback
    // and when creating the tree.
    let signal = Arc::new(
        f.signal("SetBrightness", ())
            .sarg::<&str, _>("type")
            .sarg::<&str, _>("driver")
            .sarg::<u32, _>("value"),
    );
    let signal2 = signal.clone();

    // We create a tree with one object path inside and make that path introspectable.
    let tree = f
        .tree(())
        .add(
            f.object_path("/org/freedesktop/login1/session/auto", ())
                .introspectable()
                .add(
                    // We add an interface to the object path...
                    f.interface("org.freedesktop.login1.Session", ())
                        .add_m(
                            // ...and a method inside the interface.
                            f.method("SetBrightness", (), move |m| {
                                // This is the callback that will be called when another peer on the bus calls our method.
                                // the callback receives "MethodInfo" struct and can return either an error, or a list of
                                // messages to send back.

                                dbg!(m.msg);

                                let data = m.msg.read3::<&str, &str, u32>()?;
                                let msg_t = data.0;

                                dbg!(msg_t);

                                // Two messages will be returned - one is the method return (and should always be there),
                                // and in our case we also have a signal we want to send at the same time.
                                Ok(vec![])

                                // Our method has one output argument and one input argument.
                            })
                            .inarg::<&str, _>("type")
                            .inarg::<&str, _>("driver")
                            .inarg::<u32, _>("value"), // We also add the signal to the interface. This is mainly for introspection.
                        )
                        .add_s(signal2), // Also add the root path, to help introspection from debugging tools.
                ),
        )
        .add(f.object_path("/", ()).introspectable());

    // We add the tree to the connection so that incoming method calls will be handled.
    tree.start_receive(&c);

    // Serve clients forever.
    loop {
        c.process(Duration::from_millis(1000))?;
    }
}
